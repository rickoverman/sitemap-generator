var Crawler = require("simplecrawler");
var swig  = require('swig');
var fs = require('fs');
var program = require('commander');


// FIX for UNABLE_TO_VERIFY_LEAF_SIGNATURE errors create by the simple crawler
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';


program
  .version('0.1.0')
  .option('-w, --website [value]', 'Website to crawl, example http://biblehub.com', website)
  .option('-f, --file [value]', 'Input json file for parsing multiple sites, example websites.json (files are stored in the `sitemaps` folder)')
  .option('-o, --output [value]', 'The output file, example sitemap.xml')
  .option('-d, --depth [value]', 'The max depth to crawl, example 3')
  .option('-s, --strip', 'Strip the url parameters')
  .parse(process.argv);


// set the website to crawl
var website = program.website ? program.website : "http://biblehub.com";

// set the output file
var outputFile = program.output ? program.output : "sitemap.xml";

// set maximum depth of links to crawl
var depth = program.depth ? program.depth : false;

// swig xml templates
var header = swig.compileFile('templates/header.xml');
var template = swig.compileFile('templates/url.xml');
var footer = swig.compileFile('templates/footer.xml');

// total links indicator
var total = 0;


var dateFormat = function()
{
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};


function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}


function extractRootDomain(url) {
    var domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            //this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    return domain;
}


function crawl(website, outputFile)
{

  // setup the crawler
  var crawler = new Crawler(website)
      .on("fetchcomplete", function (queueItem, responseBuffer, response) {

            total++;

            // print url on console
            console.log('['+total+']['+queueItem.depth+'] '+queueItem.url);

            // load swig template and write it to file
            var output = template({
                url: queueItem.url,
                date: dateFormat(new Date(), "yyyy-mm-dd'T'HH:MM:ss") // 2018-04-13T06:16:17+00:00
            });
            fs.appendFile(outputFile, output, function (err) {
              if (err) throw err;
            });

        })
      .on('complete', function(){

        // finish up the output file on completion
        fs.appendFile(outputFile, footer(), function (err) {
          if (err) throw err;
        });

      })
      .on('fetchclienterror', function(err){

        console.log(err);

      })
      .on('fetcherror', function(err){

        console.log(err);

      });

  // add crawler conditions
  var conditionID = crawler.addFetchCondition(function(queueItem, referrerQueueItem, callback) {
      callback(null, !queueItem.path.match(/\.pdf|.jpg|.css|.png|.gif|.svg|.eot|.woff|.js|.xml$/i));
  });
  // crawler.removeFetchCondition(conditionID);
  if(program.strip) crawler.stripQuerystring = true;
  // crawler.timeout = 6000000;
  // crawler.ignoreInvalidSSL = true;
  // crawler.interval = 1000;
  // crawler.userAgent = 'Node/SimpleCrawler';
  // crawler.maxConcurrency = 10;
  // crawler.initialProtocol = "https";
  // crawler.parseScriptTags=false;

  if(depth) crawler.maxDepth = depth;

  // create new output file
  fs.writeFile(outputFile, header(), function (err) {
    if (err) throw err;

    // start the crawler
    crawler.start();

  });


  // check if CTRL-C is pressed to finish up de xml file
  process.on('SIGINT', function() {

      // finish up the output file
      fs.appendFile(outputFile, footer(), function (err) {
        if (err) throw err;
        process.exit();
      });

  });

}

if(program.file){

  var websites = require('./'+program.file);
  websites.forEach(function(w){

    crawl(w, 'sitemaps/' + extractRootDomain(w)+ '.sitemap.xml');

  });

}else{

  crawl(website, outputFile);

}





/**

<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>http://www.example.com/foo.html</loc>
    <image:image>
       <image:loc>http://example.com/image.jpg</image:loc>
       <image:caption>Honden spelen poker</image:caption>
    </image:image>
  </url>
</urlset>

**/
