# sitemap-generator

Crawl a single website to a single output file.

```
node index.js -w http://biblehub.com -o sitemap.xml

```

Crawl multiple websites defined in `websites.json` with a max depth of 2 levels, the output files are stored in the `sitemaps` folder.

```
node index.js --depth 2 --file websites.json

```

Strip the parameters from the url , example ?id=1 etc.

```
node index.js -w http://biblehub.com -o sitemap.xml --strip

```

Show the usage information.

```
node index.js --help

```
